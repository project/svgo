<?php

namespace Drupal\svgo;

/**
 * SVGO Optimizer interface.
 */
interface SvgoOptimizerInterface {

  /**
   * Optimize an SVG.
   *
   * @param string $svg
   *  The SVG content.
   *
   * @return string
   *   The optimized SVG content.
   */
  public function optimize(string $svg): string;
}
