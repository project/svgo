<?php

namespace Drupal\svgo\Optimizer;

use Drupal\svgo\SvgoOptimizerInterface;

/**
 * Remove XML instructions optimizer.
 */
class RemoveXmlInstructions implements SvgoOptimizerInterface {

  /**
   * {@inheritdoc}
   */
  public function optimize(string $svg): string {
    // A simple regex can do the trick here.
    return preg_replace('/<\?xml.*?>/i', '', $svg, 1);
  }

}
