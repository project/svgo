<?php

namespace Drupal\svgo\Optimizer;

use Drupal\svgo\SvgoOptimizerInterface;

/**
 * Class for removing comments from SVG code.
 */
class SvgCommentRemover implements SvgoOptimizerInterface {

  /**
   * Optimize a SVG.
   *
   * @param string $svg
   *  The SVG content.
   *
   * @return string
   *   The optimized SVG.
   */
  public function optimize(string $svg) : string {
    // Remove SVG comments using regular expression.
    $svg = preg_replace('/<!--(.*?)-->/s', '', $svg);

    return $svg;
  }

}
