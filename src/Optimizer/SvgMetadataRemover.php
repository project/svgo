<?php

namespace Drupal\svgo\Optimizer;

use Drupal\svgo\SvgoOptimizerInterface;

/**
 * Class for removing <metadata> from SVG code.
 */
class SvgMetadataRemover implements SvgoOptimizerInterface {

  /**
   * Optimize a SVG.
   *
   * @param string $svg
   *   The SVG content.
   *
   * @return string
   *   The optimized SVG.
   */
  public function optimize(string $svg) : string {
    // Remove SVG metadata using regular expression.
    return preg_replace('/<metadata[^>]*>.*?<\/metadata>/si', '', $svg);

  }

}
