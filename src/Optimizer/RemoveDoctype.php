<?php

namespace Drupal\svgo\Optimizer;

use Drupal\svgo\SvgoOptimizerInterface;

/**
 * Remove doctype optimizer.
 */
class RemoveDoctype implements SvgoOptimizerInterface {

  /**
   * {@inheritdoc}
   */
  public function optimize(string $svg): string {
    // A simple regex can do the trick here.
    return preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', '', $svg);
  }

}
