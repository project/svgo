<?php

namespace Drupal\Tests\svgo\Unit;

use Drupal\svgo\Optimizer\RemoveXmlInstructions;
use Drupal\Tests\UnitTestCase;

/**
 * Test the RemoveXmlInstructions optimizer.
 *
 * @group svgo
 */
class RemoveXmlInstructionsTest extends UnitTestCase {

  /**
   * Tests XML instructions removal.
   */
  public function testRemoveXmlInstructions() {
    $optimizer = new RemoveXmlInstructions();
    $original = <<<SVG
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="5cm" height="4cm" version="1.1"
     xmlns="http://www.w3.org/2000/svg" xmlns:xlink= "http://www.w3.org/1999/xlink">
  <image xlink:href="firefox.jpg" x="0" y="0" height="50px" width="50px"/>
</svg>
SVG;

    $expected = <<<SVG

<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="5cm" height="4cm" version="1.1"
     xmlns="http://www.w3.org/2000/svg" xmlns:xlink= "http://www.w3.org/1999/xlink">
  <image xlink:href="firefox.jpg" x="0" y="0" height="50px" width="50px"/>
</svg>
SVG;

    $this->assertEquals($expected, $optimizer->optimize($original));
  }

}
