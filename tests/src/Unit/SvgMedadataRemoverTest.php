<?php

namespace Drupal\Tests\svgo\Unit;

use Drupal\svgo\Optimizer\SvgMetadataRemover;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the SvgMetadataRemover class.
 *
 * @group svgo
 */
class SvgMedadataRemoverTest extends UnitTestCase {

  /**
   * Tests removing <metadata> from SVG code.
   */
  public function testRemoveMetadata() {
    $svgCode = '<!-- begin svg cercle -->
                 <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><metadata id="metadata">
                 <cc:Work rdf:about=""><dc:format>image/svg+xml</dc:format></cc:Work>
                 </metadata>
                 <circle cx="50" cy="50" r="50"/>
                 </svg>
                <!-- end svg cercle -->';
    $expected = '<!-- begin svg cercle -->
                 <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                 <circle cx="50" cy="50" r="50"/>
                 </svg>
                <!-- end svg cercle -->';
    $remover = new SvgMetadataRemover();
    $result = $remover->optimize($svgCode);

    $this->assertEquals($result, $expected);
  }

}
