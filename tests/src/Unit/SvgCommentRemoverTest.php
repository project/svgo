<?php

namespace Drupal\Tests\svgo\Unit;

use Drupal\svgo\Optimizer\SvgCommentRemover;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the SvgCommentRemover class.
 *
 * @group svgo
 */
class SvgCommentRemoverTest extends UnitTestCase {

  /**
   * Tests removing comments from SVG code.
   */
  public function testRemoveComments() {
    $svgCode = '<!-- begin svg cercle -->
                 <svg viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                 <circle cx="50" cy="50" r="50"/>
                 </svg>
                <!-- end svg cercle -->';

    $remover = new SvgCommentRemover();
    $result = $remover->optimize($svgCode);

    $this->assertStringNotContainsString('<!--', $result);
    $this->assertStringNotContainsString('-->', $result);
  }

}
